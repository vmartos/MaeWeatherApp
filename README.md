# MaeWeatherApp
Xamarin.Forms simple weather app that uses OpenWeatherMap API with the OpenWeatherNet .NetStandard library.

OpenWeatherNet build status: [![Build Status](https://travis-ci.org/vmartos/OpenWeatherNet.svg?branch=master)](https://travis-ci.org/vmartos/OpenWeatherNet)
